//
//  ApplicationViewController.m
//  League of Stats
//
//  Created by CS 262 - Tyler Daughtry on 10/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Starting point for application.  Launches the navigation controller

#import "Application.h"

@interface Application ()

@end

static dispatch_once_t oncePredicate;
static UINavigationController *_navController = nil;

@implementation Application

+ (void)pushViewController:(UIViewController *)vc animated:(bool)b {
    [_navController pushViewController:vc animated:b];
}

+ (UINavigationController *)navController {
    //Only do initialization once
    dispatch_once(&oncePredicate, ^{
        _navController = [[UINavigationController alloc] init];
    });
    
    return _navController;
}


//TODO We may not need these methods, pending further thought...

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
