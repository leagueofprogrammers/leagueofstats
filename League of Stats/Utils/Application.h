//
//  ApplicationViewController.h
//  League of Stats
//
//  Created by CS 262 - Tyler Daughtry on 10/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Starting point for application Launches the navigation controller 

#import <UIKit/UIKit.h>

@interface Application : UIViewController

+ (void)pushViewController:(UIViewController*)vc animated:(bool)b;
+ (UINavigationController *)navController;

@end
