//
//  HelpViewController.m
//  League of Stats
//
//  Created by Colin Gesink on 12/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import "HelpViewController.h"


@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UIView *ContentView;

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self setTitle:@"Help"];
    
    [_ScrollView addSubview:_ContentView];
    [self.view addSubview:_ScrollView];
    
    
    [self.navigationController setNavigationBarHidden:NO];

    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self.ContentView attribute:NSLayoutAttributeLeading relatedBy:0 toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    
    [self.view addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self.ContentView attribute:NSLayoutAttributeTrailing relatedBy:0 toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
    
    [self.view addConstraint:rightConstraint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
