//
//  ThreadedCollectionViewCell.m
//  League of Stats
//
//  Created by Tyler Dougherty on 10/24/14.
//  Authors: Tyler D
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import "ThreadedCollectionViewCell.h"

@implementation ThreadedCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
    [self.contentView addSubview:_imageView];
}

- (void)dealloc
{
    _imageView.image = nil;
}

#pragma mark - NSURLConnectionDataDelegate methods

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    NSURLResponse *response = cachedResponse.response;
    if ([response isKindOfClass:NSHTTPURLResponse.class]) return cachedResponse;
    
    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
    NSDictionary *headers = HTTPResponse.allHeaderFields;
    if (headers[@"Cache-Control"]) return cachedResponse;
    
    NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
    modifiedHeaders[@"Cache-Control"] = @"max-age=60";
    NSHTTPURLResponse *modifiedResponse = [[NSHTTPURLResponse alloc]
                                           initWithURL:HTTPResponse.URL
                                           statusCode:HTTPResponse.statusCode
                                           HTTPVersion:@"HTTP/1.1"
                                           headerFields:modifiedHeaders];
    
    cachedResponse = [[NSCachedURLResponse alloc]
                      initWithResponse:modifiedResponse
                      data:cachedResponse.data
                      userInfo:cachedResponse.userInfo
                      storagePolicy:cachedResponse.storagePolicy];
    return cachedResponse;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    UIImage *icon = [UIImage imageWithData:data];
    _imageView = [[UIImageView alloc] initWithImage:icon];
    [self.contentView addSubview:_imageView];
    
    [_imageView setFrame:self.contentView.bounds];
    
    [UIView beginAnimations:@"Fade in" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2];
    [self setAlpha:1];
    [UIView commitAnimations];
    
//    NSThread *t = [[NSThread alloc] initWithTarget:self selector:@selector(fadeInCell) object:nil];
//    [t start];
}

- (void)fadeInCell
{
    [UIView beginAnimations:@"Fade in" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2];
    [self setAlpha:1];
    [UIView commitAnimations];
}

@end
