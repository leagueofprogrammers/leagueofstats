//
//  ThreadedCollectionViewCell.h
//  League of Stats
//
//  Created by Tyler Dougherty on 10/24/14.
//  Authors: Tyler D
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Creates cell to display an individual champion in champion menu

#import <UIKit/UIKit.h>

@interface ThreadedCollectionViewCell : UICollectionViewCell <NSURLConnectionDataDelegate>

@property (strong, nonatomic) NSURLConnection *conn;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) NSURL *currentURL;

@end
