//
//  ChampionDetailViewController.h
//  League of Stats
//
//  Created by Tyler Dougherty on 11/3/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Champion.h"

@interface ChampionDetailViewController : UIViewController

- (instancetype)init:(Champion *)champion;

@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *foregroundView;
@property (strong, nonatomic) IBOutlet UIView *fadedView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) Champion *champion;

@end
