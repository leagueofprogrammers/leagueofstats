//
//  ChampionViewController.m
//  League of Stats
//
//  Created by CS 262 on 10/20/14.
//  Authors: Tyler D, Collin G
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for Champion View

#import "ChampionViewController.h"
#import "iLOL.h"
#import "ThreadedCollectionViewCell.h"
#import "Application.h"
#import "ChampionDetailViewController.h"

@interface ChampionViewController ()

@end

@implementation ChampionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    [self setTitle:@"Champions"];
    
    //Test loading from the server
    iLOL *server = [[iLOL alloc] init];
    _championData = [server getChampions];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView registerClass:[ThreadedCollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    //Fixes resizing issue related to UINavigationController
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Delegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    //hard coded for now, will get needed data from database
    // Nevermind, we only need one section - CG
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
     //Should be total number or champions in database
    return _championData.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
     //hard coded for now, will get needed data from database, then build cells
    ThreadedCollectionViewCell* newCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    //Clean old cell views and hide (to be redisplayed)
    [newCell setAlpha:0];
    if (!newCell.imageView) {
        newCell.imageView = [[UIImageView alloc] initWithFrame:newCell.contentView.bounds];
        [newCell.contentView addSubview:newCell.imageView];
    }
    
    newCell.backgroundColor = [UIColor blueColor];
    Champion *champ = [_championData objectAtIndex:[indexPath row]];
    newCell.currentURL = champ.iconURL;
    
    //TODO: add in caching later
    [self downloadFromURL:champ.iconURL to:nil completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            if (newCell.currentURL == champ.iconURL) {
                newCell.imageView.image = image;
                
                [UIView animateWithDuration:0.4 animations:^{
                    [newCell setAlpha:1];
                }];
            }
        }
    }];
    
    return newCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChampionDetailViewController *dvc = [[ChampionDetailViewController alloc] init:[_championData objectAtIndex:[indexPath row]]];
    [Application pushViewController:dvc animated:YES];
}

- (void)downloadFromURL:(NSURL *)url to:(NSString *)filePath completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:10];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [[UIImage alloc] initWithData:data];
            completionBlock(YES, image);
        } else {
            completionBlock(NO, nil);
        }
    }];
}

@end
