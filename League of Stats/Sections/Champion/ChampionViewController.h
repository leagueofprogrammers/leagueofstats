//
//  ChampionViewController.h
//  League of Stats
//
//  Created by CS 262 on 10/20/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for Champion View

#import <UIKit/UIKit.h>



@interface ChampionViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewCell *cell;
@property (strong, nonatomic) NSArray *championData;

@end
