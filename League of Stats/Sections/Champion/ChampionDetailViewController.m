//
//  ChampionDetailViewController.m
//  League of Stats
//
//  Created by Tyler Dougherty on 11/3/14.
//  Author: Tyler D, Collin G
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import "ChampionDetailViewController.h"

@interface ChampionDetailViewController ()

@end

@implementation ChampionDetailViewController

- (instancetype)init:(id)champion {
    self = [self initWithNibName:@"ChampionDetailViewController" bundle:nil];
    
    _champion = champion;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Make the views automatically take into account the navbar
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) //Not sure if this actually does anything...
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //Set faded view background color
    [_fadedView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    
    //Set the scroller to a lighter color
    [_scrollView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    
    //Load the background image
    NSString *urlString = [NSString stringWithFormat:@"http://ddragon.leagueoflegends.com/cdn/img/champion/loading/%@_0.jpg", _champion.key];
    UIImage *backgroundImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
    [_backgroundView setImage:backgroundImage];

    //Set up the name  label
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_contentView addSubview:nameLabel];
//    [nameLabel setText:];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@, %@", _champion.name, _champion.title]];
    [text addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0, [text length]}];
    [text addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:24] range:(NSRange){0, [text length]}];
    nameLabel.attributedText = text;
    [nameLabel setNumberOfLines:0];
    [nameLabel setTextColor:[UIColor whiteColor]];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    NSDictionary *nameDict = NSDictionaryOfVariableBindings(_scrollView, nameLabel);
    NSArray *h_name = [NSLayoutConstraint constraintsWithVisualFormat:@"|-[nameLabel]-|" options:0 metrics:nil views:nameDict];
    NSArray *v_name = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[nameLabel(>=40)]" options:0 metrics:nil views:nameDict];
    [_contentView addConstraints:h_name];
    [_contentView addConstraints:v_name];

    //Create holder views
    UILabel *prevLabel = nameLabel;
    
    //Create all of the ability views
    for (int q = 0; q < _champion.spells.count; q++) {
        //Create the views
        NSDictionary *spellDict = [_champion.spells objectAtIndex:q];
        NSURL *spellImgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://ddragon.leagueoflegends.com/cdn/4.20.1/img/spell/%@", [[spellDict objectForKey:@"image"] objectForKey:@"full"]]];
        UIImage *spellImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:spellImgURL]];
        UIImageView *spellImgView = [[UIImageView alloc] initWithImage:spellImg];
        spellImgView.translatesAutoresizingMaskIntoConstraints = NO;
        UILabel *spellDescription = [[UILabel alloc] init];
        spellDescription.translatesAutoresizingMaskIntoConstraints = NO;
        [spellDescription setText:[NSString stringWithFormat:@"%@: %@", [spellDict objectForKey:@"name"], [spellDict objectForKey:@"description"]]];
        [spellDescription setTextColor:[UIColor whiteColor]];
        [spellDescription setNumberOfLines:0]; //Allows text to decide the number of lines
        
        //Add the views to the main view
        [_contentView addSubview:spellImgView];
        [_contentView addSubview:spellDescription];
        
        //Set up constraints for this view
        NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(spellImgView, spellDescription, prevLabel);
        NSArray *h_constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-[spellImgView(60)]-20-[spellDescription]-|" options:0 metrics:nil views:viewsDictionary];
        NSArray *v_constraints1 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[prevLabel]-20-[spellImgView]" options:0 metrics:nil views:viewsDictionary];
        NSString *formatString = @"V:[prevLabel]-20-[spellDescription(>=spellImgView)]";
        if (q == _champion.spells.count - 1)
            formatString = [NSString stringWithFormat:@"%@-|", formatString];
        NSArray *v_constraints2 = [NSLayoutConstraint constraintsWithVisualFormat:formatString options:0 metrics:nil views:viewsDictionary];
        
        //Add the constraints
        [_contentView addConstraints:h_constraints];
        [_contentView addConstraints:v_constraints1];
        [_contentView addConstraints:v_constraints2];
        
        //Assign temps for use next iteration
        prevLabel = spellDescription;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end