//
//  SummonerCompareViewController.h
//  League of Stats
//
//  Created by Andrew Peterson on 11/12/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SummonerCompareViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *profileImage1;
@property (weak, nonatomic) IBOutlet UILabel *normalWinsLabel1;
@property (weak, nonatomic) IBOutlet UILabel *summonerNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel1;
@property (weak, nonatomic) IBOutlet UILabel *rankedWinsLabel1;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam3Image1;
@property (weak, nonatomic) IBOutlet UIImageView *badgeSoloImage1;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam5Image1;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam3Label1;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam5Label1;
@property (weak, nonatomic) IBOutlet UILabel *rankedSolo5Label1;
@property (weak, nonatomic) IBOutlet UILabel *rankedKDALabel1;

@property (weak, nonatomic) IBOutlet UIImageView *profileImage2;
@property (weak, nonatomic) IBOutlet UILabel *normalWinsLabel2;
@property (weak, nonatomic) IBOutlet UILabel *summonerNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel2;
@property (weak, nonatomic) IBOutlet UILabel *rankedWinsLabel2;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam3Image2;
@property (weak, nonatomic) IBOutlet UIImageView *badgeSoloImage2;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam5Image2;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam3Label2;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam5Label2;
@property (weak, nonatomic) IBOutlet UILabel *rankedSolo5Label2;
@property (weak, nonatomic) IBOutlet UILabel *rankedKDALabel2;

- (instancetype)initWithSummoner:(NSString *)summoner1 andSummoner:(NSString *)summoner2;

@end
