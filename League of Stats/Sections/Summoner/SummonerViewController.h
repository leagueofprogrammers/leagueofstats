//
//  SummonerViewController.h
//  League of Stats
//
//  Created by CS 262 on 10/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for Summoner view

#import <UIKit/UIKit.h>

@interface SummonerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *normalWinsLabel;
@property (weak, nonatomic) IBOutlet UILabel *summonerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedWinsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam3Image;
@property (weak, nonatomic) IBOutlet UIImageView *badgeSoloImage;
@property (weak, nonatomic) IBOutlet UIImageView *badgeTeam5Image;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam3Label;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam3Loss;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam5Label;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam5Loss;
@property (weak, nonatomic) IBOutlet UILabel *rankedSolo5Label;
@property (weak, nonatomic) IBOutlet UILabel *rankedSoloLoss;
@property (weak, nonatomic) IBOutlet UILabel *rankedKDALabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedMinionKillsLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedNeutralMinionsKilledLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedTurretsKilledLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedChampionKillsLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedAssistsLabel;

@property (weak, nonatomic) IBOutlet UILabel *rankedTeam3DescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedSolo5DescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankedTeam5DescriptionLabel;



@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

- (IBAction)bookmarkButton:(id)sender;

- (instancetype)initWithSummoner:(NSString *)summoner;

- (void) toggleSearch;

- (IBAction)Help:(id)sender;

@end
