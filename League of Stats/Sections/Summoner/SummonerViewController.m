//
//  SummonerViewController.m
//  League of Stats
//
//  Created by CS 262 on 10/2/14.
//  Authors: Tyler D, Colin G, Andy P
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for Summoner View

#import "SummonerViewController.h"
#import "iLOL.h"
#import "Application.h"
#import "SummonerCompareViewController.h"
#import "HelpViewController.h"

@interface SummonerViewController ()

@property (retain, nonatomic) NSString *summoner;

@end

@implementation SummonerViewController

- (IBAction)bookmarkButton:(id)sender {
    //Check for null summoner
    if(self.summonerNameLabel.text && self.summonerNameLabel.text.length > 0){
        NSUserDefaults *bookmarks = [NSUserDefaults standardUserDefaults];
        NSArray *imageArray = [bookmarks objectForKey:@"Image"];
        NSArray *nameArray = [bookmarks objectForKey:@"Name"];
        if(imageArray == nil){
            imageArray =[[NSArray alloc] init];
        }
        if (nameArray == nil) {
            nameArray  = [[NSArray alloc] init];
        }
        
        //If bookmark array contains less than 8 entries
        if( nameArray.count <= 8)
        {
            UIImage *bookmarkImage = self.profileImage.image;
            NSData *imageData = UIImageJPEGRepresentation(bookmarkImage, 1);
        
            NSString *name= self.summonerNameLabel.text;
        
        
            NSMutableArray *newImage = [imageArray mutableCopy];
            
            //Store the bookmark into NSUserdefauls
            if(bookmarkImage){
                [newImage addObject:imageData];
                [bookmarks setObject:newImage forKey:@"Image"];
            } else {
                UIImage *placeholderImage = [UIImage imageNamed:@"blankBadge.png"];
                NSData *placeholderData = UIImageJPEGRepresentation(placeholderImage, 1);
                [newImage addObject:placeholderData];
                [bookmarks setObject:newImage forKey:@"Image"];
            }
        
        
            NSMutableArray *newName = [nameArray mutableCopy];
            [newName addObject:name];
            [bookmarks setObject:newName forKey:@"Name"];
        
        
        
            NSLog(@"Data Saved!");
            //create alert
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bookmark Saved!" message:@"You successfully created a new bookmark!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }

}

- (instancetype)initWithSummoner:(NSString *)summoner {
    self = [super initWithNibName:@"SummonerViewController" bundle:nil];
    
    _summoner = summoner;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    // hide search
    float delta = self.searchBar.frame.size.height;
    self.searchBar.frame = CGRectOffset(self.searchBar.frame, 0.0, -delta);
    self.searchBar.hidden = YES;
    
    
    [_searchBar setDelegate:self];
    [_searchBar setReturnKeyType:UIReturnKeySearch];
    
    
    iLOL *server = [[iLOL alloc] init];
    Summoner *summoner = [server getSummoner:_summoner];
    [self setTitle:@"Summoner"];
    [self.profileImage setImage:[summoner getProfileIcon]];
    NSString *team3badge = summoner.rankedTeam3v3Tier;
    NSString *team5badge = summoner.rankedTeam5v5Tier;
    NSString *solo5badge = summoner.rankedSolo5v5Tier;
    [self.badgeTeam3Image setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team3badge]]];
    [self.badgeSoloImage setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", solo5badge]]];
    [self.badgeTeam5Image setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team5badge]]];
    [self.summonerNameLabel setText:summoner.name];
    [self.normalWinsLabel setText:[NSString stringWithFormat:@"Normal Wins: %li", summoner.normalWins]];
    [self.levelLabel setText:[NSString stringWithFormat:@"Level %li", summoner.summonerLevel]];
    [self.rankedSolo5Label setText:[NSString stringWithFormat:@"%li", summoner.rankedSolo5v5Wins]];
    [self.rankedSoloLoss setText:[NSString stringWithFormat:@"%li", summoner.rankedSolo5v5Losses]];
    [self.rankedTeam5Label setText:[NSString stringWithFormat:@"%li", summoner.rankedTeam5v5Wins]];
    [self.rankedTeam5Loss setText:[NSString stringWithFormat:@"%li", summoner.rankedTeam5v5Losses]];
    [self.rankedTeam3Label setText:[NSString stringWithFormat:@"%li", summoner.rankedTeam3v3Wins]];
    [self.rankedTeam3Loss setText:[NSString stringWithFormat:@"%li", summoner.rankedTeam3v3Losses]];
    long totalRankedWins = summoner.rankedSolo5v5Wins + summoner.rankedTeam3v3Wins + summoner.rankedTeam5v5Wins;
    long totalRankedLosses = summoner.rankedTeam5v5Losses + summoner.rankedTeam3v3Losses + summoner.rankedSolo5v5Losses;
    [self.rankedWinsLabel setText:[NSString stringWithFormat:@"Ranked: %li/%li", totalRankedWins, totalRankedLosses]];
    [self.rankedKDALabel setText:(summoner.rankedKDA != -1) ? [NSString stringWithFormat:@"KDA: %f", summoner.rankedKDA] : @"KDA: Perfect!"];
    [self.rankedChampionKillsLabel setText:[NSString stringWithFormat:@"Champions Killed: %li", summoner.totalChampionKills]];
    [self.rankedAssistsLabel setText:[NSString stringWithFormat:@"Assists: %li", summoner.totalAssists]];
    [self.rankedMinionKillsLabel setText:[NSString stringWithFormat:@"Minions Killed: %li", summoner.totalMinionKills]];
    [self.rankedNeutralMinionsKilledLabel setText:[NSString stringWithFormat:@"Neutral Minions Killed: %li", summoner.totalNeutralMinionsKilled]];
    [self.rankedTurretsKilledLabel setText:[NSString stringWithFormat:@"Turrets Killed: %li", summoner.totalTurretsKilled]];
    
    [self.rankedTeam3DescriptionLabel setText: [summoner.rankedTeam3v3Tier capitalizedString]];
    [self.rankedSolo5DescriptionLabel setText: [summoner.rankedSolo5v5Tier capitalizedString]];
    [self.rankedTeam5DescriptionLabel setText: [summoner.rankedTeam5v5Tier capitalizedString]];
    
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Compare" style: UIBarButtonItemStyleBordered  target:self action:@selector(toggleSearch)];
    //initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(toggleSearch)];
    self.navigationItem.rightBarButtonItem = searchButton;
    
    //Hide the keyboard tapping outside the search bar
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
}

- (void)dismissKeyboard {
    [_searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// adopted from http://dashasalo.com/2012/02/09/ios-make-search-bar-slide-in-and-out-on-button-click/
- (void) toggleSearch {
    {
        

        // check if toolbar was visible or hidden before the animation
        BOOL isHidden = [self.searchBar isHidden];
       
        
        if (isHidden) {
            self.searchBar.hidden = NO;
        }
        
        // run animation 0.7 second and no delay
        [UIView animateWithDuration:0.7 delay: 0.0 options: UIViewAnimationOptionCurveEaseIn animations:^{
            // move search bar delta units up or down
            self.searchBar.frame = CGRectOffset(self.searchBar.frame, 0.0, 0.0);
        } completion:^(BOOL finished) {
            //if the bar was visible then hide it
            if (!isHidden) {
                self.searchBar.hidden = YES;
            } 
        }];
    }
}

- (IBAction)Help:(id)sender {
    HelpViewController *helpVC = [[HelpViewController alloc] init];
    [Application pushViewController:helpVC animated:true];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self openCompareSummoner:nil];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    BOOL isHidden = [self.searchBar isHidden];
    if (!isHidden){
        self.searchBar.hidden = YES;
    }
}

- (IBAction)openCompareSummoner:(UIButton *)sender {
        SummonerCompareViewController *otherVC = [[SummonerCompareViewController alloc] initWithSummoner:self.summoner andSummoner:_searchBar.text];
        [Application pushViewController:otherVC animated:true];

}


@end
