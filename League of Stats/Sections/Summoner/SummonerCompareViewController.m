//
//  SummonerCompareViewController.m
//  League of Stats
//
//  Created by Andrew Peterson on 11/12/14.
//  Authors: Andy P
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import "SummonerCompareViewController.h"
#import "iLOL.h"

@interface SummonerCompareViewController ()

@property (retain, nonatomic) NSString *summoner1;
@property (retain, nonatomic) NSString *summoner2;

@end

@implementation SummonerCompareViewController

- (instancetype)initWithSummoner:(NSString *)summoner1 andSummoner:(NSString *)summoner2 {
    self = [super initWithNibName:@"SummonerCompareViewController" bundle:nil];
    
    _summoner1 = summoner1;
    _summoner2 = summoner2;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    //Summoner for top half of the screen.
    iLOL *server = [[iLOL alloc] init];
    Summoner *summoner1 = [server getSummoner:_summoner1];
    Summoner *summoner2 = [server getSummoner:_summoner2];
    [self setTitle:@"Compare Summoners"];
    [self.profileImage1 setImage:[summoner1 getProfileIcon]];
    NSString *team3badge1 = summoner1.rankedTeam3v3Tier;
    NSString *team5badge1 = summoner1.rankedTeam5v5Tier;
    NSString *solo5badge1 = summoner1.rankedSolo5v5Tier;
    [self.badgeTeam3Image1 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team3badge1]]];
    [self.badgeSoloImage1 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", solo5badge1]]];
    [self.badgeTeam5Image1 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team5badge1]]];
    [self.summonerNameLabel1 setText:summoner1.name];
    [self.normalWinsLabel1 setText:[NSString stringWithFormat:@"Normal Wins: %li", summoner1.normalWins]];
    [self.levelLabel1 setText:[NSString stringWithFormat:@"Level %li", summoner1.summonerLevel]];
    [self.rankedSolo5Label1 setText:[NSString stringWithFormat:@"%li/%li", summoner1.rankedSolo5v5Wins, summoner1.rankedSolo5v5Losses]];
    [self.rankedTeam5Label1 setText:[NSString stringWithFormat:@"%li/%li", summoner1.rankedTeam5v5Wins, summoner1.rankedTeam5v5Losses]];
    [self.rankedTeam3Label1 setText:[NSString stringWithFormat:@"%li/%li", summoner1.rankedTeam3v3Wins, summoner1.rankedTeam3v3Losses]];
    long totalRankedWins1 = summoner1.rankedSolo5v5Wins + summoner1.rankedTeam3v3Wins + summoner1.rankedTeam5v5Wins;
    long totalRankedLosses1 = summoner1.rankedTeam5v5Losses + summoner1.rankedTeam3v3Losses + summoner1.rankedSolo5v5Losses;
    [self.rankedWinsLabel1 setText:[NSString stringWithFormat:@"Ranked: %li/%li", totalRankedWins1, totalRankedLosses1]];
    [self.rankedKDALabel1 setText:(summoner1.rankedKDA != -1) ? [NSString stringWithFormat:@"KDA: %f", summoner1.rankedKDA] : @"KDA: Perfect!"];
    
    //Summoner for bottom half of screen
    [self.profileImage2 setImage:[summoner2 getProfileIcon]];
    NSString *team3badge2 = summoner2.rankedTeam3v3Tier;
    NSString *team5badge2 = summoner2.rankedTeam5v5Tier;
    NSString *solo5badge2 = summoner2.rankedSolo5v5Tier;
    [self.badgeTeam3Image2 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team3badge2]]];
    [self.badgeSoloImage2 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", solo5badge2]]];
    [self.badgeTeam5Image2 setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@_BADGE.png", team5badge2]]];
    [self.summonerNameLabel2 setText:summoner2.name];
    [self.normalWinsLabel2 setText:[NSString stringWithFormat:@"Normal Wins: %li", summoner2.normalWins]];
    [self.levelLabel2 setText:[NSString stringWithFormat:@"Level %li", summoner2.summonerLevel]];
    [self.rankedSolo5Label2 setText:[NSString stringWithFormat:@"%li/%li", summoner2.rankedSolo5v5Wins, summoner2.rankedSolo5v5Losses]];
    [self.rankedTeam5Label2 setText:[NSString stringWithFormat:@"%li/%li", summoner2.rankedTeam5v5Wins, summoner2.rankedTeam5v5Losses]];
    [self.rankedTeam3Label2 setText:[NSString stringWithFormat:@"%li/%li", summoner2.rankedTeam3v3Wins, summoner2.rankedTeam3v3Losses]];
    long totalRankedWins2 = summoner2.rankedSolo5v5Wins + summoner2.rankedTeam3v3Wins + summoner2.rankedTeam5v5Wins;
    long totalRankedLosses2 = summoner2.rankedTeam5v5Losses + summoner2.rankedTeam3v3Losses + summoner2.rankedSolo5v5Losses;
    [self.rankedWinsLabel2 setText:[NSString stringWithFormat:@"Ranked: %li/%li", totalRankedWins2, totalRankedLosses2]];
    [self.rankedKDALabel2 setText:(summoner2.rankedKDA != -1) ? [NSString stringWithFormat:@"KDA: %f", summoner2.rankedKDA] : @"KDA: Perfect!"];
}

@end
