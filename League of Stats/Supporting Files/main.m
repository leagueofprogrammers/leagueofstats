//
//  main.m
//  League of Stats
//
//  Created by Andrew Peterson on 9/12/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
