//
//  AppDelegate.h
//  League of Stats
//
//  Created by Andrew Peterson on 9/12/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
