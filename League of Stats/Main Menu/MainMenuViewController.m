//
//  MainMenuViewController.m
//  League of Stats
//
//  Created by CS 262 on 10/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for main menu.  Contains all the actions and oulets and methods for UI elements for main menu


#import "MainMenuViewController.h"
#import "Application.h"
#import "SummonerViewController.h"
#import "ChampionViewController.h"
#import "SummonerCompareViewController.h"
#import "HelpViewController.h"



@interface MainMenuViewController ()

@end

@implementation MainMenuViewController
@synthesize itemtoBeDeleted;
@synthesize defaults;
@synthesize bookmarkImages;
@synthesize bookmarkNames;

- (instancetype)init {
    self = [self initWithNibName:@"MainMenuViewController" bundle:nil];
    
    return self;
}

//Loads view and handles startup requirments for view
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _firstTime = [NSUserDefaults standardUserDefaults];
    if ([_firstTime objectForKey:@"FirstTime"] == nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome!" message:@"Welcome to the League Dossier App!  Here you can view statistics for champions and summoners!  Create a bookmark from the Summoner page.  Press and hold the bookmark icon to delete the bookmark!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    [_firstTime setObject:@"firsttime" forKey:@"FirstTime"];
    
    [self setTitle:@"Main Menu"];
    
    [_summonerSearchBar setDelegate:self];
    [_summonerSearchBar setReturnKeyType:UIReturnKeySearch];
    
    //Hide the keyboard tapping outside the search bar
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [_searchBar resignFirstResponder];
}

- (void) viewWillAppear:(BOOL)animated{
      [self.navigationController setNavigationBarHidden:YES];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    
    defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
    NSArray *bookmarkImages = [defaults objectForKey:@"Image"];
    NSArray *bookmarkNames = [defaults objectForKey:@"Name"];

    
    if (bookmarkImages != nil){
        for (int i = 0; i < bookmarkImages.count; i++){
            NSData *imageData = [bookmarkImages objectAtIndex:i];
            UIImage *image = [UIImage imageWithData:imageData];
            NSString *name = [bookmarkNames objectAtIndex:i];
            NSString *key = [NSString stringWithFormat:@"bookmark%d", i+1];
            UIButton *button = [self valueForKey:key];
            /*
            CGRect  textRect = CGRectMake(0, 0, size/4, size/4);
            UILabel *nameLabel = [[UILabel alloc] initWithFrame:textRect];
            [nameLabel setText:name];
            [nameLabel setBackgroundColor:[UIColor whiteColor]];
           
           
           
            CGRect  viewRect = CGRectMake(0, 0, size, size);
            UIView *buttonView = [[UIView alloc] initWithFrame:viewRect];
            [buttonView setBackgroundColor: [UIColor colorWithPatternImage:image]];
            [buttonView addSubview:nameLabel];
             [button addSubview:buttonView];
            */
            
            [button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
            [button setBackgroundImage:image forState:(UIControlStateNormal) ];
            [button setTitle:name forState:UIControlStateNormal];
            
           
            
        }
        
    }
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Handles action for "Search Summoner" button
- (IBAction)openSummoner:(UIButton *)sender {
    if([_summonerSearchBar.text rangeOfString:@"/"].location == NSNotFound)
    {
        SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner:_summonerSearchBar.text];
        [Application pushViewController:otherVC animated:true];
    }
    else
    {
        NSArray *summoners = [_summonerSearchBar.text componentsSeparatedByString:@"/"];
        SummonerCompareViewController *otherVC = [[SummonerCompareViewController alloc] initWithSummoner:summoners[0] andSummoner:summoners[1]];
        [Application pushViewController:otherVC animated:true];
    }
}

#pragma mark - Actions and Delegates for various buttons on screen

// Handles action for "View Champions" Button
- (IBAction)openChampions:(UIButton *)sender {
    ChampionViewController *otherVC = [[ChampionViewController alloc] init];
    [Application pushViewController:otherVC animated:true];
}

- (IBAction)Help:(UIButton *)sender {
    HelpViewController *helpVC = [[HelpViewController alloc] init];
    [Application pushViewController:helpVC animated:true];
}

- (IBAction)newsButtonPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://na.leagueoflegends.com/en/news/"]];
}

#pragma mark - Search Bar Delegate methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self openSummoner:nil];
}


#pragma mark - Bookmark Button Actions - ccg
- (IBAction)bookmark1:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark1.currentTitle];
    [Application pushViewController:otherVC animated:true];
    
}

- (IBAction)bookmark2:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark2.currentTitle];
    [Application pushViewController:otherVC animated:true];

}

- (IBAction)bookmark3:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark3.currentTitle];
    [Application pushViewController:otherVC animated:true];

}

- (IBAction)bookmark4:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark4.currentTitle];
    [Application pushViewController:otherVC animated:true];
}

- (IBAction)bookmark5:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark5.currentTitle];
    [Application pushViewController:otherVC animated:true];
}

- (IBAction)bookmark6:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark6.currentTitle];
    [Application pushViewController:otherVC animated:true];
}

- (IBAction)bookmark7:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark7.currentTitle];
    [Application pushViewController:otherVC animated:true];
}

- (IBAction)bookmark8:(id)sender {
    SummonerViewController *otherVC = [[SummonerViewController alloc] initWithSummoner: self.bookmark8.currentTitle];
    [Application pushViewController:otherVC animated:true];
}


/**
 handels the alert view asking user for confirmation to delete bookmark
 @param alertView 
    alert view to show
 @param buttonIndex
    index of button user selected
 */
- (void) alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self deleteBookmark:itemtoBeDeleted];
        [self viewDidAppear:true];
    }
}

#pragma mark - bookmark fuctions - ccg

/**
 Deletes the image and title of a bookmark button and updates the user defaults
 
 @param deleteIndex
 Index of the bookmark to delete
 */
-(void) deleteBookmark: (NSInteger) deleteIndex{
    defaults = [NSUserDefaults standardUserDefaults];
    NSArray *imageArray = [defaults objectForKey:@"Image"];
    NSArray *nameArray = [defaults objectForKey:@"Name"];
    
    NSMutableArray *newImage = [imageArray mutableCopy];
    [newImage removeObjectAtIndex:deleteIndex];
    [defaults setObject:newImage forKey:@"Image"];
    
    NSMutableArray *newName = [nameArray mutableCopy];
    [newName removeObjectAtIndex:deleteIndex];
    [defaults setObject:newName forKey:@"Name"];
    
    UIImage *image =[UIImage imageNamed:@"bookmark.png"];
    
    
    //Reset buttons and icons
    for (int i = 0; i < 8; i++){
        NSString *key = [NSString stringWithFormat:@"bookmark%d", i+1];
        UIButton *button = [self valueForKey:key];
        [button setBackgroundImage:image forState:(UIControlStateNormal) ];
        [button setTitle:@"" forState:UIControlStateNormal];
        
    }

    [self viewDidAppear:true];
}



#pragma mark - Long Tap Gestures - ccg

//Right now, all are separate methods... need to look into if we can create one method and pass the button
//edit... Since each is a indiviudal gesture, dont think it is possible.

/** 
 Handler for long tap function on bookmark button 1.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected1:(UIGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark1.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)0;
        }
    }
    
}

/**
 Handler for long tap function on bookmark button 2.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected2:(UIGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark2.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)1;
        }
    }
    
}

/**
 Handler for long tap function on bookmark button 3.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected3:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark3.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this  bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)2;
        }
    }
}

/**
 Handler for long tap function on bookmark button 4.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected4:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark4.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)3;
        }
    }
}

/**
 Handler for long tap function on bookmark button 5.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected5:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark5.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)4;
        }
    }
}

/**
 Handler for long tap function on bookmark button 6.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected6:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark6.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)5;
        }
    }
    
}

/**
 Handler for long tap function on bookmark button 7.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected7:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark7.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)6;
        }
    }
    
}

/**
 Handler for long tap function on bookmark button 8.  Create an alert view asking user for confermation to delte bookmakr
 
 @param sender
 Long tap gesture that is the sender
 
 */
- (IBAction)tapDetected8:(UILongPressGestureRecognizer *)sender {
    NSLog(@"Long Press!");
    if (self.bookmark8.currentTitle != nil){
        if (UIGestureRecognizerStateEnded == sender.state){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete?" message:@"Yes will delete this bookmark." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alert show];
            itemtoBeDeleted = (NSInteger)7;
        }
    }
    
}

    
    
@end
