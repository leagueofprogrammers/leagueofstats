//
//  MainMenuViewController.h
//  League of Stats
//
//  Created by CS 262 - Tyler Daughtry on 10/2/14.
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//
//  Controller for main menu.  Contains all the actions and oulets and methods for UI elements for main menu

#import <UIKit/UIKit.h>

@interface MainMenuViewController : UIViewController <UISearchBarDelegate>

@property NSInteger itemtoBeDeleted;
@property NSUserDefaults* defaults;
@property NSUserDefaults* firstTime;
@property NSArray *bookmarkImages;
@property NSArray *bookmarkNames;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UISearchBar *summonerSearchBar;
- (IBAction)newsButtonPressed:(id)sender;
- (IBAction)tapDetected1:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected2:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected3:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected4:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected5:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected6:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected7:(UILongPressGestureRecognizer *)sender;
- (IBAction)tapDetected8:(UILongPressGestureRecognizer *)sender;

- (IBAction)openSummoner:(UIButton *)sender;
- (IBAction)openChampions:(UIButton *)sender;
- (IBAction)Help:(UIButton *)sender;


- (IBAction)bookmark1:(id)sender;
- (IBAction)bookmark2:(id)sender;
- (IBAction)bookmark3:(id)sender;
- (IBAction)bookmark4:(id)sender;
- (IBAction)bookmark5:(id)sender;
- (IBAction)bookmark6:(id)sender;
- (IBAction)bookmark7:(id)sender;
- (IBAction)bookmark8:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *bookmark1;
@property (weak, nonatomic) IBOutlet UIButton *bookmark2;
@property (weak, nonatomic) IBOutlet UIButton *bookmark3;
@property (weak, nonatomic) IBOutlet UIButton *bookmark4;
@property (weak, nonatomic) IBOutlet UIButton *bookmark5;
@property (weak, nonatomic) IBOutlet UIButton *bookmark6;
@property (weak, nonatomic) IBOutlet UIButton *bookmark7;
@property (weak, nonatomic) IBOutlet UIButton *bookmark8;


@end
