# Welcome

## Vision ##
League of Legends had taken the online gaming community by storm.  The "E-Sport" attracts millions of players from around the world every day.  Just like any other sport, users accumulate statistics and rankings throughout game play.  These statistics are important when analyzing opponents as well as personal performance. We aim to create a functional and intuitive iOS application that allows users to access statistics information from the League of Legends game database. Our application will present this
data through an intelligently organized GUI and concise display.  We hope our application will  provide players with a useful tool to improve their gameplay.  We endeavor to create the most functional, usable and accurate League of Legends statistics application on the market.



#Contents#

##Project Management##
###[Task Board](https://trello.com/b/zswczYb9/cs262b-league-of-programers) ###

###[Management Sheet](https://docs.google.com/a/students.calvin.edu/spreadsheet/ccc?key=0AmZhyIpUI2YNdFZ3ajBHRDlkVG1pZzJ4NFgxeXpPdEE&usp=drive_web#gid=0)###

##* Analysis & Design##
** Include your competitive analysis here.
** Link your n+1 design models here (either in a single UML file or in separate models).
** Link your supplemental spec here.

##* Implementation##
** Link the different portions of your code repository here.

##* Testing##
** Link all your test plans and reports here. This includes system and user tests.

##* Presentation##
** Link your presentation materials here.