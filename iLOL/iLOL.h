//
//  iLOL.h
//  iLOL
//
//  Created by CS 262 -Tyler Daughtry  on 9/22/14.
//  Copyright (c) 2014 CS 262. All rights reserved.
//
//  The API to connect to the RIOT database for League stats

#import <UIKit/UIKit.h>

//Class imports
#import "Summoner.h"
#import "Champion.h"

@interface iLOL : NSObject

- (Summoner *)getSummoner:(NSString *)name;
- (NSArray *)getChampions;

@end
