//
//  Champion.h
//  League of Stats
//
//  Created by Tyler Dougherty on 10/23/14.
//  Authors: Tyler D
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Champion : NSObject <NSURLConnectionDataDelegate>

@property (atomic) long ID;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *lore;
@property (strong, nonatomic) NSString *partype;
@property (strong, nonatomic) NSString *blurb;
@property (strong, nonatomic) NSURL *iconURL;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) NSArray *spells;

- (instancetype)init:(NSDictionary *)dict;

@end
