//
//  Champion.m
//  League of Stats
//
//  Created by Tyler Dougherty on 10/23/14.
//  Authors: Tyler D
//  Copyright (c) 2014 League of Programmers. All rights reserved.
//

#import "Champion.h"

@implementation Champion

#define API_KEY @"eb7fd599-6684-4d35-a682-2e9a131fbf63" //Put this in a constants file eventually


/**
 Creats a chamion object from given dictionary
 
 @param dict
    NSDictionary of champion information
 @return 
    champion created from dict
 */
- (instancetype)init:(NSDictionary *)dict
{
    self = [self init];
    
    _ID = [[dict valueForKey:@"id"] longValue];
    _iconURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://ddragon.leagueoflegends.com/cdn/4.20.1/img/champion/%@", [[dict objectForKey:@"image"] objectForKey:@"full"]]];
    _key = [dict objectForKey:@"key"];
    _name = [dict objectForKey:@"name"];
    _title = [dict objectForKey:@"title"];
    _lore = [dict objectForKey:@"lore"];
    _partype = [dict objectForKey:@"partype"];
    _blurb = [dict objectForKey:@"blurb"];
    
    NSMutableArray *mut = [[NSMutableArray alloc] init];
    for (NSDictionary *spell in [dict objectForKey:@"spells"]) {
        [mut addObject:spell];
    }
    _spells = [NSArray arrayWithArray:mut];
    
    return self;
}

@end
