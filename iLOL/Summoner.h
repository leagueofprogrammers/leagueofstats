//
//  SummonerDTO.h
//  iLOL
//
//  Created by CS 262 - Tyler Daughtry on 10/9/14.
//  Copyright (c) 2014 CS 262. All rights reserved.
//
//  Creates a summoner object from database query

#import <UIKit/UIKit.h>

@interface Summoner : NSObject

@property (atomic) long ID;
@property (strong, atomic) NSString *name;
@property (atomic) long profileIconID;
@property (atomic) long revisionDate;
@property (atomic) long summonerLevel;

//Hacked in for now
@property (atomic) long normalWins;
@property (atomic) long rankedSolo5v5Wins;
@property (atomic) long rankedSolo5v5Losses;
@property (atomic) long rankedTeam5v5Wins;
@property (atomic) long rankedTeam5v5Losses;
@property (atomic) long rankedTeam3v3Wins;
@property (atomic) long rankedTeam3v3Losses;

@property (atomic) long rankedSolo5v5ChampionKills;
@property (atomic) long rankedSolo5v5Deaths;
@property (atomic) long rankedSolo5v5Assists;
@property (atomic) long rankedTeam5v5ChampionKills;
@property (atomic) long rankedTeam5v5Deaths;
@property (atomic) long rankedTeam5v5Assists;
@property (atomic) long rankedTeam3v3ChampionKills;
@property (atomic) long rankedTeam3v3Deaths;
@property (atomic) long rankedTeam3v3Assists;
@property (atomic) double rankedKDA;
@property (atomic) long totalChampionKills;
@property (atomic) long totalDeaths;
@property (atomic) long totalAssists;

@property (atomic) long rankedTeam3v3MinionKills;
@property (atomic) long rankedTeam5v5MinionKills;
@property (atomic) long rankedSolo5v5MinionKills;
@property (atomic) long totalMinionKills;

@property (atomic) long rankedTeam3v3NeutralMinionsKilled;
@property (atomic) long rankedTeam5v5NeutralMinionsKilled;
@property (atomic) long rankedSolo5v5NeutralMinionsKilled;
@property (atomic) long totalNeutralMinionsKilled;

@property (atomic) long rankedTeam3v3TurretsKilled;
@property (atomic) long rankedTeam5v5TurretsKilled;
@property (atomic) long rankedSolo5v5TurretsKilled;
@property (atomic) long totalTurretsKilled;

@property (strong, atomic) NSString *rankedSolo5v5Tier;
@property (strong, atomic) NSString *rankedTeam5v5Tier;
@property (strong, atomic) NSString *rankedTeam3v3Tier;
//end

- (instancetype)init:(long)ID name:(NSString *)name icon:(long)iconID revisionDate:(long)revisionDate summonerLevel:(long)level;
- (instancetype)init:(NSDictionary *)dict;
- (UIImage *)getProfileIcon;

@end
