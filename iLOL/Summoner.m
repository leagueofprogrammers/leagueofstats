//
//  SummonerDTO.m
//  iLOL
//
//  Created by CS 262 -Tyler D on 10/9/14.
//  Authors: Andy P, Tyler D
//  Copyright (c) 2014 CS 262. All rights reserved.
//
//  Creates a summoner object from database query

#import "Summoner.h"

@implementation Summoner

#define API_KEY @"eb7fd599-6684-4d35-a682-2e9a131fbf63" //Put this in a constants file eventually

- (instancetype)init:(long)ID name:(NSString *)name icon:(long)iconID revisionDate:(long)revisionDate summonerLevel:(long)level {
    self = [self init];
    
    _ID = ID;
    _name = name;
    _profileIconID = iconID;
    _revisionDate = revisionDate;
    _summonerLevel = level;
    
    return self;
}

- (instancetype)init:(NSDictionary *)dict {
    self = [self init];
    
    _ID = [[dict objectForKey:@"id"] longValue];
    _name = [dict objectForKey:@"name"];
    _profileIconID = [[dict objectForKey:@"profileIconId"] longValue];
    _revisionDate = [[dict objectForKey:@"revisionDate"] longValue];
    _summonerLevel = [[dict objectForKey:@"summonerLevel"] longValue];
    _rankedSolo5v5Tier = @"BLANK";
    _rankedTeam5v5Tier = @"BLANK";
    _rankedTeam3v3Tier = @"BLANK";
    
    //Hacked in for now
    NSString *url = [NSString stringWithFormat:@"https://na.api.pvp.net/api/lol/na/v1.3/stats/by-summoner/%li/summary?season=SEASON4&api_key=%@", _ID, API_KEY];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
    
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:&error];
    NSArray *stats = [data objectForKey:@"playerStatSummaries"];
    
    NSLog(@"%@", stats);
    
    for (NSDictionary *dict in stats) {
        if ([[dict objectForKey:@"playerStatSummaryType"] isEqualToString:@"Unranked"]) {
            //Only normal wins are stored, not losses
            _normalWins = [[dict objectForKey:@"wins"] longValue];
        }
        if ([[dict objectForKey:@"playerStatSummaryType"] isEqualToString:@"RankedSolo5x5"]) {
            //Get number of wins and losses
            _rankedSolo5v5Wins = [[dict objectForKey:@"wins"] longValue];
            _rankedSolo5v5Losses = [[dict objectForKey:@"losses"] longValue];
            
            //Other in game stats stored directly into database.
            NSDictionary *aStats = [dict objectForKey:@"aggregatedStats"];
            _rankedSolo5v5ChampionKills = [[aStats objectForKey:@"totalChampionKills"] longValue];
            _rankedSolo5v5Deaths = [[aStats objectForKey:@"totalDeathsPerSession"] longValue];
            _rankedSolo5v5Assists = [[aStats objectForKey:@"totalAssists"] longValue];
            _rankedSolo5v5MinionKills = [[aStats objectForKey:@"totalMinionKills"] longValue];
            _rankedSolo5v5NeutralMinionsKilled = [[aStats objectForKey:@"totalNeutralMinionsKilled"] longValue];
            _rankedSolo5v5TurretsKilled = [[aStats objectForKey:@"totalTurretsKilled"] longValue];
        }
        if ([[dict objectForKey:@"playerStatSummaryType"] isEqualToString:@"RankedTeam5x5"]) {
            _rankedTeam5v5Wins = [[dict objectForKey:@"wins"] longValue];
            _rankedTeam5v5Losses = [[dict objectForKey:@"losses"] longValue];
            
            NSDictionary *aStats = [dict objectForKey:@"aggregatedStats"];
            _rankedTeam5v5ChampionKills = [[aStats objectForKey:@"totalChampionKills"] longValue];
            _rankedTeam5v5Deaths = [[aStats objectForKey:@"totalDeathsPerSession"] longValue];
            _rankedTeam5v5Assists = [[aStats objectForKey:@"totalAssists"] longValue];
            _rankedTeam5v5MinionKills = [[aStats objectForKey:@"totalMinionKills"] longValue];
            _rankedTeam5v5NeutralMinionsKilled = [[aStats objectForKey:@"totalNeutralMinionsKilled"] longValue];
            _rankedTeam5v5TurretsKilled = [[aStats objectForKey:@"totalTurretsKilled"] longValue];
        }
        if ([[dict objectForKey:@"playerStatSummaryType"] isEqualToString:@"RankedTeam3x3"]) {
            _rankedTeam3v3Wins = [[dict objectForKey:@"wins"] longValue];
            _rankedTeam3v3Losses = [[dict objectForKey:@"losses"] longValue];
            
            NSDictionary *aStats = [dict objectForKey:@"aggregatedStats"];
            _rankedTeam3v3ChampionKills = [[aStats objectForKey:@"totalChampionKills"] longValue];
            _rankedTeam3v3Deaths = [[aStats objectForKey:@"totalDeathsPerSession"] longValue];
            _rankedTeam3v3Assists = [[aStats objectForKey:@"totalAssists"] longValue];
            _rankedTeam3v3MinionKills = [[aStats objectForKey:@"totalMinionKills"] longValue];
            _rankedTeam3v3NeutralMinionsKilled = [[aStats objectForKey:@"totalNeutralMinionsKilled"] longValue];
            _rankedTeam3v3TurretsKilled = [[aStats objectForKey:@"totalTurretsKilled"] longValue];
        }
        
    }
    
    //Extra calculations for totals
    _totalChampionKills = _rankedTeam5v5ChampionKills + _rankedTeam3v3ChampionKills + _rankedSolo5v5ChampionKills;
    _totalDeaths = _rankedSolo5v5Deaths + _rankedTeam3v3Deaths + _rankedTeam5v5Deaths;
    _totalAssists = _rankedTeam5v5Assists + _rankedTeam3v3Assists + _rankedSolo5v5Assists;
    //KDA is calculated by total kills and assists over deaths. If no deaths, -1 signifies perfect KDA
    _rankedKDA = (_totalDeaths != 0) ? (_totalChampionKills+_totalAssists)/_totalDeaths : -1;
    
    _totalMinionKills = _rankedTeam5v5MinionKills + _rankedTeam3v3MinionKills + _rankedSolo5v5MinionKills;
    _totalNeutralMinionsKilled = _rankedTeam5v5NeutralMinionsKilled + _rankedTeam3v3NeutralMinionsKilled + _rankedSolo5v5NeutralMinionsKilled;
    _totalTurretsKilled = _rankedTeam5v5TurretsKilled + _rankedTeam3v3TurretsKilled + _rankedSolo5v5TurretsKilled;
    
    //Grabbing tiers
    url = [NSString stringWithFormat:@"https://na.api.pvp.net/api/lol/na/v2.5/league/by-summoner/%li?api_key=%@", _ID, API_KEY];
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
    
    urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *tierData = [[NSJSONSerialization JSONObjectWithData:urlData options:0 error:&error] objectForKey:[NSString stringWithFormat:@"%lu", _ID]];
    
    //This will loop through the teams of a summoner and store the highest rank for each gamemode.
    for( NSDictionary *LeagueDTO in tierData)
    {
        if([[LeagueDTO objectForKey: @"queue"] isEqualToString:@"RANKED_SOLO_5x5"] && [self getTierRank:_rankedSolo5v5Tier] < [self getTierRank:[LeagueDTO objectForKey: @"tier"]])
        {
            _rankedSolo5v5Tier = [LeagueDTO objectForKey: @"tier"];
        }
        else if([[LeagueDTO objectForKey: @"queue"] isEqualToString:@"RANKED_TEAM_5x5"] && [self getTierRank:_rankedTeam5v5Tier] < [self getTierRank:[LeagueDTO objectForKey: @"tier"]])
        {
            _rankedTeam5v5Tier = [LeagueDTO objectForKey: @"tier"];
        }
        else if([[LeagueDTO objectForKey: @"queue"] isEqualToString:@"RANKED_TEAM_3x3"] && [self getTierRank:_rankedTeam3v3Tier] < [self getTierRank:[LeagueDTO objectForKey: @"tier"]])
        {
            _rankedTeam3v3Tier = [LeagueDTO objectForKey: @"tier"];
        }
    }
    
    //end
    
    return self;
}


/** 
 Fetches Summoner profile icon from database
 
 @return the icon as UIImage
 */
- (UIImage *)getProfileIcon {
    UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString
        stringWithFormat:@"http://ddragon.leagueoflegends.com/cdn/4.20.1/img/profileicon/%li.png", _profileIconID]]]];
    
    return img;
}

/** 
 Returns a numeric value for a given tier. Higher tiers have higher numbers.
 
 @param tier 
    tier name
 @return
    returns numeric value for tier
 */
 
- (int)getTierRank: (NSString *)tier {
    if([tier isEqualToString:@"CHALLENGER"])
    {
        return 7;
    }
    else if([tier isEqualToString:@"MASTER"])
    {
        return 6;
    }
    else if([tier isEqualToString:@"DIAMOND"])
    {
        return 5;
    }
    else if([tier isEqualToString:@"PLATINUM"])
    {
        return 4;
    }
    else if([tier isEqualToString:@"GOLD"])
    {
        return 3;
    }
    else if([tier isEqualToString:@"SILVER"])
    {
        return 2;
    }
    else if([tier isEqualToString:@"BRONZE"])
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



@end