//
//  iLOL.m
//  iLOL
//
//  Created by CS 262 -Tyler D  on 9/22/14.
//  Authors: Tyler D
//  Copyright (c) 2014 CS 262. All rights reserved.
//
// The API to connect to the RIOT database for League stats

#import "iLOL.h"

@implementation iLOL

#define API_KEY @"eb7fd599-6684-4d35-a682-2e9a131fbf63" //Put this in a constants file eventually

/**
    Fetches and returns summoner from database.  Returns a summoner object
 @param name
    name of summoner to be fetched
 
 @return
    summoner object
 */
- (Summoner *)getSummoner:(NSString *)name
{
    NSString *url = [NSString stringWithFormat:@"http://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/%@?api_key=%@", name, API_KEY];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:&error];
    data = [data objectForKey:[[data allKeys] objectAtIndex:0]];
    
    Summoner *summ = [[Summoner alloc] init:data];
    
    return summ;
}

/**
 Fetches and returns an array of champions
 
 @return
    an Array of champion objects
 */
- (NSArray *)getChampions
{
    NSString *url = [NSString stringWithFormat:@"https://na.api.pvp.net/api/lol/static-data/na/v1.2/champion?champData=image,spells&api_key=%@", API_KEY];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:10];
    
    NSData *urlData;
    NSURLResponse *response;
    NSError *error;
    
    urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *data = [NSJSONSerialization JSONObjectWithData:urlData options:0 error:&error];
    data = [data objectForKey:@"data"];
    
    
    NSMutableArray *back = [[NSMutableArray alloc] init];
    NSArray *keys = [data allKeys];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *key in keys)
    {
        [back addObject:[[Champion alloc] init:[data objectForKey:key]]];
    }
    
    return [NSArray arrayWithArray:back];
}

@end
